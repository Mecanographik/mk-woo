<header class="banner centered-navigation{!! App\nav_class() !!}" role="banner">
  <div class="container centered-navigation-wrapper">
    <a href="{{ home_url('/') }}" title="{{ get_bloginfo('name', 'display') }}" class="mobile-logo">
      <img src="@asset(images/logo-emeraude-emotions.svg)" alt="Logo {!! App\site_name() !!}">
    </a>
    <a href="javascript:void(0)" id="js-centered-navigation-mobile-menu" class="centered-navigation-mobile-menu">MENU</a>
    <nav class="nav-primary" role="navigation">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_id' => 'js-centered-navigation-menu', 'menu_class' => 'nav centered-navigation-menu show']) !!}
      @endif
    </nav>
  </div>
</header> 
