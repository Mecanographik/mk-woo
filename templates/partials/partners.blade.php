<section class="content-info" id="partners">
  <div class="container">
   <h2>Nos partenaires</h2>

   <div class="grid-items-lines">
	  <a href="javascript:void(0)" class="grid-item">
	    <img src="@asset(images/suzuki.png)" alt="Suzuki">
	    <h1>Suzuki</h1>
	    <!-- <p>text partner</p> -->
	  </a>
	  <a href="javascript:void(0)" class="grid-item">
	    <img src="@asset(images/cras-nautique.png)" alt="Cras Nautique Paimpol">
	    <h1>Cras Nautique Paimpol</h1>
	  </a>
	  <a href="javascript:void(0)" class="grid-item">
	    <img src="@asset(images/ribcraft.png)" alt="Ribcraft">
	    <h1>Ribcraft</h1>
	  </a>
	 
	  <div class="right-cover"></div>
	  <div class="bottom-cover"></div>

	</div>

  </div>
</section>
