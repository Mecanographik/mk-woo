<div id="js-parallax-window" class="page-cover parallax-window">
  <div class="parallax-static-content">
  	<h1>{!! App\site_name() !!}</h1>
    <h2>{!! App\title() !!}</h2>
    <p class="cta"><a href="#featured" class="btn large rounded orange">Je réserve</a>
<br><div class="fb-like" style="padding-top:30px;" data-href="https://www.facebook.com/emeraudeemotions/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
    </p>
  </div>
  <div id="js-parallax-background" class="parallax-background"></div>
</div>
