<article @php(post_class())>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
  </header>
  <div class="entry-summary">
  	@if (is_post_type_archive())
    @php(the_content())
    @else
    @if (has_post_thumbnail())
    <img src="{{ get_the_post_thumbnail_url( get_the_id(), 'large' ) }}" alt="image {{ get_the_title() }}">
    @endif
    {{-- @include('partials/entry-meta') --}}
    @php(the_excerpt())
    @endif
  </div>
</article>
