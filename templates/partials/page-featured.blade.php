<?php
/*
* example snippet if needed to get variation from product id, outside the loop
*
* $product_id would be for eg. : 'decouverte'

$args = array(
'post_type'     => 'product_variation',
'post_status'   => array( 'private', 'publish' ),
'numberposts'   => -1,
'orderby'       => 'menu_order',
'order'         => 'asc',
'post_parent'   => $product_id // $post->ID 
);
$variations = get_posts( $args ); 
echo "<pre>"; print_r($variations); echo "</pre>"; 

*/
?>

<section id="featured">
	<h2>Sélectionnez votre excursion</h2>
	<p class="subtitle">Partez à la découverte des sites incontournables de la Côte d’Emeraude, du Cap Fréhel à St-Malo.</p>

	<!--
	<div class="flash-alert">
	  <span>Profitez de notre offre de lancement : <strong>-10% sur les 100 premières réservations!</strong></span>
	</div>
	-->
	
	<div class="cards" id="featured">
	  <div class="card">
	    <div class="card-image">
	      <a href="{{ get_permalink(woo_id('decouverte')) }}"><img src="{{ get_the_post_thumbnail_url( woo_id('decouverte'), 'large' ) }}" alt=""></a>
	    </div>
	    <div class="card-header">
	      {{ get_the_title(woo_id('decouverte')) }}
	    </div>
	    <div class="card-subheader">
	    	{!! get_the_product_attribute(woo_id('decouverte'),'Durée') !!}
	    	{!! woo_price(woo_id('decouverte')) !!}
	    </div>
	    <div class="card-copy">
	      	<p>{{ get_the_excerpt(woo_id('decouverte')) }}</p>
	    	<p class="cta"><a href="{{ get_permalink(woo_id('decouverte')) }}" class="btn turquoise">Réserver</a></p>
	    </div>
	  </div>

	  <div class="card shelfend">
	    <div class="card-image">
	      <a href="{{ get_permalink(woo_id('inoubliable')) }}"><img src="{{ get_the_post_thumbnail_url( woo_id('inoubliable'), 'large' ) }}" alt="Excursion"></a>
	    </div>
	    <div class="card-header">
	       {{ get_the_title(woo_id('inoubliable')) }}
	    </div>
	    <div class="card-subheader">
	     {!! get_the_product_attribute(woo_id('inoubliable'),'Durée') !!}
	     {!! woo_price(woo_id('inoubliable')) !!}
	    </div>
	    <div class="card-copy">
	      	<p>{{ get_the_excerpt(woo_id('inoubliable')) }}</p>
	    	<p class="cta"><a href="{{ get_permalink(woo_id('inoubliable')) }}" class="btn orange">Réserver</a></p>
	    </div>
	  </div>

	  <div class="card">
	    <div class="card-image">
	     <a href="{{ get_permalink(woo_id('incontournable')) }}"><img src="{{ get_the_post_thumbnail_url( woo_id('incontournable'), 'large' ) }}" alt=""></a>
	    </div>
	    <div class="card-header">
	      {{ get_the_title(woo_id('incontournable')) }}
	    </div>
	    <div class="card-subheader">
	     {!! get_the_product_attribute(woo_id('incontournable'),'Durée') !!}
	     {!! woo_price(woo_id('incontournable')) !!}
	    </div>
	    <div class="card-copy">
	      <p>{{ get_the_excerpt(woo_id('incontournable')) }}</p>
	      <p class="cta"><a href="{{ get_permalink(woo_id('incontournable')) }}" class="btn turquoise">Réserver</a></p>
	    </div>
	  </div>
	</div>

	<p class="subtitle">Vous souhaitez réserver en groupe ou effectuer une location privatisée ? <a href="{{ home_url('/excursions/a-la-carte/') }}">En savoir plus</a></p>
	<p class=""><div class="fb-like" style="padding-top:10px;" data-href="https://www.facebook.com/emeraudeemotions/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
		<br><small>Crédit photos : ©Dominique Denis - Avec l'autorisation des Editions Armoric</small></p>
</section>