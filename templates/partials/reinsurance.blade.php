<section id="reinsurance">
<div class="grid-items-lines">
  <a href="javascript:void(0)" class="grid-item">
    <img src="@asset(images/icon-ios-time-outline.svg)" alt="Réservation en temps réel">
    <h1>Réservation en temps réel</h1>
    <p>Les places réservées sont décomptées à chaque nouvelle commande. Vous êtes donc sûr de disposer d’une place à bord à partir du moment où vous pouvez la commander.</p>
  </a>
  <a href="javascript:void(0)" class="grid-item">
    <img src="@asset(images/icon-lock.svg)" alt="Paiement sécurisé">
    <h1>Paiement sécurisé</h1>
    <p>Notre site Web est entièrement chiffré en SSL (HTTPS). Les transactions sont réalisées uniquement sur les serveurs de notre solution de paiement Stripe.com</p>
  </a>
  <a href="javascript:void(0)" class="grid-item">
    <img src="@asset(images/icon-ios-heart-outline.svg)" alt="Satisfaction garantie">
    <h1>Satisfaction garantie</h1>
    <p>Nos excursions permettent de découvrir des sites naturels incontournables et des paysages époustouflants de la Côte d’Emeraude d’une façon inédite.</p>
  </a>
 
  <div class="right-cover"></div>
  <div class="bottom-cover"></div>
</div>
</section>