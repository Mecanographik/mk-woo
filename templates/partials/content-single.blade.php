<article @php(post_class())>
  <header>
    <h1 class="entry-title">{{ get_the_title() }}</h1>
    
  </header>
  <div class="entry-content">
    @if (has_post_thumbnail())
    <img src="{{ get_the_post_thumbnail_url( get_the_id(), 'large' ) }}" alt="image {{ get_the_title() }}">
    @endif
    @if (!is_singular('faq'))
    {{-- @include('partials/entry-meta') --}}
    @endif
    @php(the_content())
  </div>
  <footer>
    {!! wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @php(comments_template('/templates/partials/comments.blade.php'))
</article>
