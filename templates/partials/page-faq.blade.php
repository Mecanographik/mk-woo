<section id="faq" class="page-content">
<h2>FAQ</h2>
@php
$args = array(
'orderby' => 'title',
'post_type' => 'faq'
);
@endphp

@query($args)
<div>
	<h3>@title</h3>
	<p>@excerpt</p>
</div>
@endquery

<p class="cta"><a href="{{ home_url('/faq/') }}" title="Consulter la FAQ" class="btn large">Toute la FAQ</a></p>

</section>