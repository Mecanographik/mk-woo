<section id="testimonial" class="parallax-window">
  <div class="parallax-static-content">
    <h2>Rejoignez l'expérience {!! App\site_name() !!}</h2>
    <h3 class="tripadvisor">
    <div class="center-content">
    <a href="https://www.tripadvisor.fr/Attraction_Review-g666785-d12270119-Reviews-Emeraude_Emotions-Saint_Cast_le_Guildo_Cotes_d_Armor_Brittany.html" title="Rejoignez {{ get_bloginfo('name', 'display') }} sur Tripadvisor" class="mobile-logo" target="_blank">
      <img src="@asset(images/logo-tripadvisor.png)" alt="Tripadvisor {!! App\site_name() !!}">
    </a>
    <span class="text-right-logo">
    	Recommandé sur <a href="https://www.tripadvisor.fr/Attraction_Review-g666785-d12270119-Reviews-Emeraude_Emotions-Saint_Cast_le_Guildo_Cotes_d_Armor_Brittany.html">Tripadvisor</a>
    	<br>
    	<a target="_blank" href="https://www.tripadvisor.fr/Attraction_Review-g666785-d12270119-Reviews-Emeraude_Emotions-Saint_Cast_le_Guildo_Cotes_d_Armor_Brittany.html">Consulter les avis</a>
    </span>
	
	</div></h3>

    <div class="grid-items-lines distant">
	  
	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
		    <img class="avatar" src="@asset(images/avatar-test-003.png)" alt="Satisfaction garantie">
		    <h1>Jean Yves BORDIER</h1>
		    <small>Maître beurrier, St-Malo</small>
		    <p>En embarquant, Ronan le skipper nous explique très sérieusement les règles de sécurité, une démarche qui rassure. Dès la sortie port, il nous décrit avec passion les oiseaux, le paysage et très vite la navigation devient passionnante et ludique.</p>
	  	</div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	    <div class="grid-item-content">
		    <img class="avatar" src="@asset(images/avatar-test-002.png)" alt="Paiement sécurisé">
		    <h1>Arnaud COSTE</h1>
		    <small>Directeur Grand Aquarium de St-Malo</small>
		    <p>Emeraude émotions est une expérience à vivre dans l’air du temps et dans l’authenticité du Marin qui veut la faire partager. Ronan est un passionné qui vous fera découvrir la Mer, un acteur engagé pour transformer vos loisirs en exploration du littoral. A ne pas manquer.</p>
	  	</div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
	    	<img class="avatar" src="@asset(images/avatar-goulven.jpg)" alt="Témoignage">
	    	<h1>Goulven CASTERET</h1>
	    	<small>Oddo&compagnie, Dir. régional Ouest</small>
	    	<p>Nous avons eu l’occasion de rencontrer Ronan à Calvi en juin 2016. Avec des mots simples, sa disponibilité et sa gentillesse, il a su faire passer des messages de sécurité mais surtout nous faire partager sa passion de la mer par des commentaires avisés tout au long de la journée.</p>
	    </div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
	    	<img class="avatar" src="@asset(images/avatar-test-001.png)" alt="Témoignage">
	    	<h1>Benoit LEVAVASSEUR</h1>
	    	<small>Capitaine catamaran, Calvi</small>
	    	<p>Confiance, professionnalisme et bonne humeur. Ronan a été mon chef mécanicien pendant la saison 2016 à Calvi, et c’est lui qui commentait la visite. Ses commentaires sont toujours instructifs, ludiques, avec une touche d’humour que les passagers apprécient.</p>
	    </div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	    <div class="grid-item-content">
		    <img class="avatar" src="@asset(images/avatar-jacques-antoine-orsini.jpg)" alt="Paiement sécurisé">
		    <h1>Jacques-antoine ORSINI</h1>
		    <small>Capitaine, pilote de surfeur, Calvi</small>
		    <p>En plus d’être bon marin j’ai pu observer chez Ronan le contact facile qu’il pouvait avoir avec les passagers. Il met toujours un point d’honneur à faire des balades agrémentées de commentaires sur la faune, la flore et l’histoire du site et en prendre plein les yeux.</p>
	  	</div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
	    	<img class="avatar" src="@asset(images/avatar-regis-herve.jpg)" alt="Témoignage">
	    	<h1>Régis HERVE</h1>
	    	<small>Chef d’entreprise, Quimper, Brest</small>
	    	<p>Tout est fait pour que les invités profitent de l'expérience avec un petit plus qui rend le moment inoubliable: anecdotes sur les lieux visités, parfaite adaptation du programme aux conditions météo…</p>
	    </div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
	    	<img class="avatar" src="@asset(images/avatar-cecile-alix.jpg)" alt="Témoignage">
	    	<h1>Cécile Alix</h1>
	    	<small>Commercante, St-Malo</small>
	    	<p>Nous avons eu l’occasion à plusieurs reprises de naviguer avec Ronan. Vu la qualité de ses balades (commentaires, sécurité…), nous attendons avec impatience qu’il nous fasse découvrir la côte d’Emeraude…</p>
	    </div>
	  </a>

	  <a href="javascript:void(0)" class="grid-item tem">
	  	<div class="grid-item-content">
	    	<img class="avatar" src="@asset(images/avatar-jean-marc-laine.jpg)" alt="Témoignage">
	    	<h1>Jean Marc LAINE</h1>
	    	<small>Centre Hospitalier, Dinan</small>
	    	<p>Lors de notre séjour en corse nous avons effectué une excursion en mer à bord d'un gros  bateau semi rigide équipé de 2 moteurs hors bord puissants. Ce bateau était piloté par mr Ronan Chartrey. Nous avons pu apprécier la qualité de pilotage, son professionnalisme.</p>
	    </div>
	  </a>

	  
	</div>

    <p class="cta">
    	<a href="#" id="show-all" class="btn large white transparent">Tous les témoignages</a>
    	&nbsp;&nbsp;<a href="https://www.tripadvisor.fr/UserReviewEdit-g666785-d12270119-ehttp:__2F____2F__www__2E__tripadvisor__2E__fr__2F__Attraction__5F__Review__2D__g666785__2D__d12270119__2D__Reviews__2D__Emeraude__5F__Emotions__2D__Saint__5F__Cast__5F__le__5F__Guildo__5F__Cotes__5F__d__5F__Armor__5F__Brittany__2E__html-Emeraude_Emotions-Saint_Cast_le_Guildo_Cotes_d_Armor_Brittany.html" class="btn large white transparent" target="_blank" title="avis sur tripadvisor">Ajouter un avis</a>
    </p>
  </div>
  <div id="js-parallax-background-testimonial" class="parallax-background"></div>
</section>
