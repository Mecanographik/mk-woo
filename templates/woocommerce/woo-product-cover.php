<?php
/**
 * The template for displaying product cover in the single-product.php template
 *
 * This template is loaded from src/woocommerce.php
 *
 * HOOK : woocommerce_before_single_product_summary
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product
?>
<style>
body {
	background: url("<?php echo get_the_post_thumbnail_url( $post->ID, 'large' ); ?>") no-repeat;
}
</style>
<section id="product-cover">
	<h1><?php the_title(); ?></h1>
	<?php if ($product->get_attribute( 'Durée' )) : ?>
	<!--<p class="short-description"><?php echo $post->post_excerpt; ?></p>-->
	<h2>Durée : <?php echo $product->get_attribute( 'Durée' ); ?> <a class="scroll-to-book orange" href="#show_stock_status">Réserver</a>
	<div class="fb-like" style="padding-left:20px;" data-href="https://www.facebook.com/emeraudeemotions/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
	</h2>
	<?php endif; ?>
</section>