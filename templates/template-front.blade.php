{{--
  Template Name: Front Page Template
--}}

@extends('layouts.base')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.page-cover')
    @include('partials.page-featured')
    @include('partials.reinsurance')
    @include('partials.page-testimonial')
    @include('partials.page-faq')
   {{-- @include('partials.content-page') --}}
  @endwhile
@endsection
