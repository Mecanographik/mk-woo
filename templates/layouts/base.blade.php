<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    @php(do_action('get_header'))
    @include('partials.header')
    <div class="wrap {!! App\wrap_class() !!}" role="document">
      <div class="content">
        @if (!is_post_type_archive('product') )
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
        @endif
      </div>
    </div>
    @if (is_post_type_archive('product') )
    @include('partials.page-featured')
    @endif
    @if ( (is_post_type_archive() OR is_page() OR is_singular('product')) AND !is_front_page() )
    @include('partials.reinsurance')
    @endif
    @include('partials.partners')
    @include('partials.footer')
    @php(do_action('get_footer'))
    @php(wp_footer())
  </body>
</html>
