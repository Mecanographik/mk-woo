<?php
/*
WooCommerce Shipping Contactfunctions
based on Plugin : https://gist.github.com/helgatheviking/c3381322bade0227d762ba1cf429271e
Description: Manage shipping fields during checkout and notify of new orders for 'revendeurs'
see: https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
*/


/**
* Manage shipping form
* @link https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/#section-6
*/

add_filter( 'woocommerce_cart_needs_shipping', '__return_true' );
//  ‘check’ a checkbox to show them and ship to a different address.
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );


 // remove tour operator add-on plugin action in email
/*
// doesnt work ;-) 
// !!!!!!!  pour le moment la fonction tour_operators_details() est commentée dans le plugin 'bkap tour operator' > print-tickets.php

function child_remove_parent_function() {
    remove_action( 'woocommerce_email_customer_details', 'tour_operators_details',  11 );
}
add_action( 'init', 'child_remove_parent_function' );
*/


?>