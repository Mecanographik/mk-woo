<?php
// extras functions

// redirect non logged in user to production site

/*
add_action('template_redirect', function () {
    
    if (is_user_logged_in()) 
    	return '';

    $redirect_to_url = 'https://cs.emeraude-emotions.com';

    // if maintenance mode is on
    $maintenance_mode = false;
    $front_page_id = get_option( 'page_on_front' );
    if ($front_page_id) {
        $front_Page_slug = get_post_field( 'post_name', $front_page_id ); // you need to setup a page 'maintenance' as the front page 
        if ($front_Page_slug == 'maintenance' ) $maintenance_mode = true; // you need to create a page 'maintenance'
    }

    // check if we are in .dev local mode
    $production = false;
    $domain = explode(".",$_SERVER['HTTP_HOST']); // create an array of the bits
    $number = count($domain); // find out how many there are
    $tld = $domain[$number-1]; // tld is last element

    if($tld == "com") { // .org/.net/.tv etc
        $production = true;
        // else we presume we are in .dev (local)
    }

    // private site mode
    if ( (!is_user_logged_in() && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php' && $production ) OR $maintenance_mode ) {
        //wp_redirect( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 301 );
        wp_redirect( $redirect_to_url );
        exit();
    }
});
*/

/*
* Wordpress login experience custom code
* 
* ================================================================
*/

// Stay logged in for longer periods
// modify the cookie expiration time by simply changing the time in seconds 
add_filter( 'auth_cookie_expiration', 'keep_me_logged_in' );
function keep_me_logged_in( $expirein ) {
    return 31556926; // 1 year in seconds
}

/**
 * Customize the wp login screen
 */ 
function new_wp_login_url() {
    return home_url();
}
add_filter('login_headerurl', 'new_wp_login_url');


function new_login_url_text() {
  $site_title = get_bloginfo('name'); 
    return $site_title;
}
add_filter('login_headertitle', 'new_login_url_text');

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo.svg);
            padding-bottom: 30px;
            background-size: cover;
            width:140px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );



/**
* Custom actions
* ==============
*/


/**
* Prefetch Commonly Used Domains and google fonts
*/

add_action('wp_head', function () {
    $output = "\r\n";
    $output.= '<link rel="dns-prefetch" href="//fonts.googleapis.com">'."\r\n";
    $output.= '<link rel="dns-prefetch"  href="//ajax.googleapis.com">'."\r\n";
    $output.= '<link rel="dns-prefetch"  href="//www.google-analytics.com">'."\r\n";
    $output.= '<link href="https://fonts.googleapis.com/css?family=Dosis:300,400" rel="stylesheet">'."\r\n\r\n";
    echo $output;
});


// We prefetch Commonly Used Domains and google fonts
add_action('wp_head',  'load_sdk_fb', 11);
function load_sdk_fb() {
  $output = "\r\n";
  $output.= '<div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.8&appId=1678114902215095";
      fjs.parentNode.insertBefore(js, fjs);';
    $output.= "}(document, 'script', 'facebook-jssdk'));</script>"."\r\n";
  
  // will output in head.php
  echo $output;
}


// We prefetch Commonly Used Domains and google fonts
add_action('wp_head',  'load_google_analytics', 12);
function load_google_analytics() {
  echo "\r\n";
  ?>
   <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96605609-1', 'auto');
  ga('send', 'pageview');
  </script>
  <?php
  
  // will output in head.php
}

?>