<?php

namespace App;


// doc: https://github.com/roots/docs/tree/sage-9/sage

// hide admin bar on all front facing pages.
add_filter('show_admin_bar', '__return_false');


/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
});


/**
 * The sage/display_sidebar filter can be used to define which conditions 
 * to enable the primary sidebar on.
*/

add_filter('sage/display_sidebar', function ($display) {
    static $display;

    isset($display) || $display = in_array(true, [
      // The sidebar will be displayed if any of the following return true
      //is_single(),
      is_singular('post'),
      //is_404(),
      //is_page_template('template-custom.php'),
      is_404()
    ]);

    return $display;
});


/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Filter the excerpt length to more characters if in some post type archives, etc...
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */

add_filter('excerpt_length', function () {
   if (is_post_type_archive('faq')) {
        return 10000;
    }
    else {
        return 60;
    }
});


/**
 * Template Hierarchy should search for .blade.php files
 */
array_map(function ($type) {
    add_filter("{$type}_template_hierarchy", function ($templates) {
        return call_user_func_array('array_merge', array_map(function ($template) {
            $transforms = [
                '%^/?(templates)?/?%' => config('sage.disable_option_hack') ? 'templates/' : '',
                '%(\.blade)?(\.php)?$%' => ''
            ];
            $normalizedTemplate = preg_replace(array_keys($transforms), array_values($transforms), $template);
            return ["{$normalizedTemplate}.blade.php", "{$normalizedTemplate}.php"];
        }, $templates));
    });
}, [
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
]);

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = array_reduce(get_body_class(), function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    echo template($template, $data);

    // Return a blank file to make WordPress happy
    return get_theme_file_path('index.php');
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', 'App\\template_path');



/**
 * Change the output title on a category page
 * @link https://developer.wordpress.org/reference/functions/get_the_archive_title/
 */

add_filter( 'get_the_archive_title', function ( $title ) {
    if (is_post_type_archive()) {
       $title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
    }
    else if( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
});

