<?php
// woocommerce extras helpers for blade templates 

/**
 * Featured products id
 * we only have 3 products (for now) so we stock the 3 ids
 * @return string
 */
function woo_id($product_slug='decouverte')
{  	
	switch ($product_slug)
	{
	    case 'decouverte': 
	    	$product_id = 38;
	   	break;
	    case 'inoubliable':
	        $product_id = 54;
	    break;
	    case 'incontournable':
	        $product_id = 55;
	    break;
	}

	// La decouverte
	return $product_id;
}


/*
 * wc_remove_related_products
 * 
 * Clear the query arguments for related products so none show.
 * Add this code to your theme functions.php file.  
 */
function wc_remove_related_products( $args ) {
	return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10); 


/**
 * Trim zeros in price decimals (no price like '38,00')
 **/
 add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

/**
 * Get product price by id
 * we only have 3 products (for now) so we stock the 3 ids
 * @return string
 */
function woo_price($product_id)
{  	
	$product = wc_get_product( $product_id );

	//$_product->get_regular_price();
	//$_product->get_sale_price();
	$product_price = $product->get_price() . '€ / pers.';

	 if ( $product->is_on_sale() ) {
	 	$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
		$product_price = '<strong>- ' . $percentage . '% </strong> ';
	 	$product_price .= '<strike>'.$product->get_regular_price().'€</strike> <strong>' . $product->get_sale_price() . '€ </strong>/ pers.';
	 }
	// La decouverte
	return $product_price;
}


// Add save percent next to sale item prices.		
add_filter( 'woocommerce_sale_price_html', 'woocommerce_custom_sales_price', 10, 2 );		
function woocommerce_custom_sales_price( $price, $product ) {			
	$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );			
	return $price . '<span class="price-discount-info"><br>' . sprintf( __(' Économisez %s', 'woocommerce' ), $percentage . '%</span>' );		
}

// show the percentage saved on the purchase instead of a static text on sale badge
add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );
function wc_custom_replace_sale_text( $html ) {
	global $product;
	$product = wc_get_product();
	$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
	$sale_info = '- ' . $percentage . '%';
    return str_replace( __( 'Sale!', 'woocommerce' ), __( $sale_info, 'woocommerce' ), $html );
}

/**
 * has_role_tour_operator 
 *
 * function to check if a user has a specific role
 * 
 * @param  string  $role    role to check against 
 * @param  int  $user_id    user id
 * @return boolean
 */

function has_role_tour_operator($role = '',$user_id = null){
    if ( is_numeric( $user_id ) )
        $user = get_user_by( 'id',$user_id );
    else
        $user = wp_get_current_user();

    if ( empty( $user ) )
        return false;

    return in_array( $role, (array) $user->roles );
}


//! defined( 'ABSPATH' ) AND exit;
/* 
Deny access for certain roles 
*/


function wp_tour_operator_no_admin_access()
{
	if(is_admin()) {
	    $redirect = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : home_url( '/' );
	    if(isset( $_SERVER['HTTP_REFERER'] ) && ($_SERVER['HTTP_REFERER'] != get_home_url()) ) {
	    	$redirect = home_url( '/' );
	    }
	    
	    if (  !defined( 'DOING_AJAX' ) && current_user_can( 'tour_operator' ) ) {
	    	 exit( wp_redirect( $redirect ) );
	    }
	}
}
add_action( 'admin_init', 'wp_tour_operator_no_admin_access', 100 );


/**
* 	Changing WooCommerce Display Price Based on User Role
*  	use the woocommerce_get_price filter hook to filter the value 
*  	based on user role and return a price accordingly
* 	@link http://wordpress.stackexchange.com/questions/111772/changing-woocommerce-display-price-based-on-user-role-category
*/

//add_filter('woocommerce_get_price', 'custom_price_tour_operator', 10, 2);

/**
 * custom_price_WPA111772 
 *
 * filter the price based on category and user role
 * @param  $price   
 * @param  $product 
 * @return 
 */
function custom_price_tour_operator($price, $product) {
    if (!is_user_logged_in()) return $price;

    //check if the product is in a category you want, let say shirts
   // if( has_term( 'shirts', 'product_cat' ,$product->ID) ) {
    //check if the user has a role of dealer using a helper function, see bellow
        if (has_role_tour_operator('tour_operator') OR has_role_tour_operator('shop_manager')){
            //give user 10% of
           // $price = $price * 0.9;

        	global $product;
			$product_id = $product->id;
			$price = 0; // make it free
			if ($product_id == 22) {
				$price = ''; // no price for "a la carte"
			}
        }
   // }
    return $price;
}


/**
 * Hides the 'Free!' price notice
 */
function hide_free_price_notice( $price ) {
	$value = "Accès revendeur";
  	return $value;
}
/*
add_filter( 'woocommerce_variable_free_price_html',  'hide_free_price_notice' );
add_filter( 'woocommerce_free_price_html',           'hide_free_price_notice' );
add_filter( 'woocommerce_variation_free_price_html', 'hide_free_price_notice' );
*/

function custom_heading_before_checkout_form() {
	if ( current_user_can('tour_operator') ) {
	echo '<div class="woocommerce-message">Vous êtes connecté en tant que revendeur.</div>';
	}
	if ( current_user_can('shop_manager') ) {
	echo '<div class="woocommerce-message">Vous êtes connecté en tant que Shop Manager.</div>';
	}
}
add_action( 'woocommerce_before_cart', 'custom_heading_before_checkout_form' );
add_action( 'woocommerce_before_checkout_form', 'custom_heading_before_checkout_form' );


/**
 * @snippet       Enable Payment Gateway for a Specific User Role | WooCommerce
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=273
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 2.4.10
 */
 
function tour_operator_gateway_enable_manager( $available_gateways ) {
	global $woocommerce;
	if ( isset( $available_gateways['cod'] ) && !current_user_can('shop_manager') && !current_user_can('tour_operator') ) {
	unset( $available_gateways['cod'] );
	}
	if ( isset( $available_gateways['stripe'] ) && (current_user_can('tour_operator') OR current_user_can('shop_manager')) ) {
	unset( $available_gateways['stripe'] );

	/**
	* Add Shipping Address for Virtual Products in WooCommerce
	* @link https://whiteleydesigns.com/add-shipping-address-virtual-products-woocommerce/
	*/

	// Add Shipping Option Back to Checkout Page
	// see also : /woocommerce/email/email-addresses.php
	add_filter( 'woocommerce_cart_needs_shipping_address', '__return_true', 50 );

	//  ‘check’ a checkbox to show them and ship to a different address.
	add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );

	}
	
	return $available_gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'tour_operator_gateway_enable_manager' );


function custom_heading_before_order_details( $order ) {
	// custom seeting added for tour_oprator wp user role
	//$tour_op_show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id() && !has_role_tour_operator('tour_operator');
	
	if ( ! wc_ship_to_billing_address_only() && ( $shipping = $order->get_formatted_shipping_address() ) ) : ?>

	<br><h3><?php _e( 'Shipping Address', 'woocommerce' ); ?></h3>
			
	<address>
		<?php echo ( $address = $order->get_formatted_shipping_address() ) ? $address : __( 'N/A', 'woocommerce' ); ?>
	</address><br>

	<?php endif;
}
//add_action( 'woocommerce_order_details_after_customer_details', 'custom_heading_before_order_details' );


/**
 * Place a cart icon with number of items and total cost in the menu bar.
 *
 * https://sridharkatakam.com/adding-cart-icon-number-items-total-cost-nav-menu-using-woocommerce/
 * Source: http://wordpress.org/plugins/woocommerce-menu-bar-cart/
 */
add_filter('wp_nav_menu_items','sk_wcmenucart', 10, 2);
function sk_wcmenucart($menu, $args) {

	// Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || 'primary_navigation' !== $args->theme_location )
		return $menu;

	ob_start();
		global $woocommerce;
		$viewing_cart = __('View your shopping cart', 'sage');
		$start_shopping = __('Start shopping', 'sage');
		$cart_url = $woocommerce->cart->get_cart_url();
		$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
		$cart_contents_count = $woocommerce->cart->cart_contents_count;
		$cart_contents = sprintf(_n('%d place', '%d places', $cart_contents_count, 'sage'), $cart_contents_count);
		$cart_total = $woocommerce->cart->get_cart_total();
		$cart_total_display = ' - '.$cart_total;
		// Uncomment the line below to hide nav menu cart item when there are no items in the cart
		// if ( $cart_contents_count > 0 ) {
			if ($cart_contents_count == 0) {
				$menu_item = '<li class="menu-item cart cart-empty"><a class="wcmenucart-contents ion-ios-basket-outline" href="'. $shop_page_url .'" title="'. $start_shopping .'">';
				$cart_total_display = '';
			} else if ($cart_contents_count == 1) {
				$menu_item = '<li class="menu-item cart one-item"><a class="wcmenucart-contents ion-ios-pricetag-outline" href="'. $cart_url .'" title="'. $viewing_cart .'">';
			} else {
				$menu_item = '<li class="menu-item cart several-items"><a class="wcmenucart-contents ion-ios-pricetags-outline" href="'. $cart_url .'" title="'. $viewing_cart .'">';
			}

			//$menu_item .= '<i class="icon ion-ios-basket-outline"></i> ';

			$menu_item .= '&nbsp; ' . $cart_contents. $cart_total_display;
			$menu_item .= '</a></li>';
		// Uncomment the line below to hide nav menu cart item when there are no items in the cart
		// }
		echo $menu_item;
	$social = ob_get_clean();
	return $menu . $social;
}



/**
* Remove WooCommerce SKUs Only on Product Pages (keep it enabled in admin)
* @link https://www.skyverge.com/blog/how-to-hide-sku-woocommerce-product-pages/
*/
function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }
    return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );



/**
 * // Woocommerce getting custom attributes based on product id and attribute's name
 * 
 * @return string
 */
function get_the_product_attribute($product_id, $attribute_name)
{  	
	global $product;
	$product = wc_get_product($product_id);
	// Get product attributes
	$attributes = $product->get_attributes();

	$attributes_html = '';

	foreach ( $attributes as $attribute ) {

		if ($attribute_name === $attribute['name']) {

			$attributes_html .=  '<div class="attribute-title '.$attribute['name'].'">'.$attribute['name']. ' : ';
	        $product_attributes = array();
	        $product_attributes = explode('|',$attribute['value']);
	       // $attributes_html .= '<div class="attribute-item">';

	        foreach ( $product_attributes as $pa ) {
	            $attributes_html .= '<span class="attribute-value ' . $pa . '">' . $pa . '</span>';
	        }
	       // $attributes_html .= '</div>';
	        $attributes_html .= '</div>';
		}
	}
	return apply_filters( 'woocommerce_get_product_attributes', $attributes_html );

	/*
	$attributes_html = 'attribute not found';
	$prod_att = array_shift( wc_get_product_terms( $product_id, 'pa_'.$attribute_name, array( 'fields' => 'names' ) ) );
	if ($prod_att) {
		$attributes_html = $prod_att;
	}
	//return $attributes_html;
	*/
}



// woocommerce templates hooks

/**
 * Featured products id
 * we only have 3 products (for now) so we stock the 3 ids
 * @return string
 */
function woo_product_cover()
{  
	get_template_part('templates/woocommerce/woo-product-cover'); 
}
add_action( 'woocommerce_before_single_product_summary', 'woo_product_cover' );


/**
 * manage woocommerce hooks in templates
 * Single Products Summary, etc...
 *
 * @see woocommerce_show_product_images()
 * @see woocommerce_show_product_thumbnails()
 */

// Remove and place image from product pages on top of main page
//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
//remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 1 );

//add_action('woocommerce_single_product_summary', 'woocommerce_show_product_thumbnails', 20);


add_filter ( 'woocommerce_product_thumbnails_columns', 'xx_thumb_cols' );
 function xx_thumb_cols() {
     return 4; // .last class applied to every 4th thumbnail
 }


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_title', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_rating', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_price', 10 );

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
//add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 20);
add_action( 'woocommerce_product_thumbnails', 'woocommerce_template_single_meta', 20 );


// tags
function woocommerce_product_loop_tags() {
    global $post, $product;
    $tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
    //echo $product->get_tags( ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'woocommerce' ) . ' ', '.</span>' );
    echo $product->get_tags( ' ', '<div class="product_meta"><span class="tagged_as">' . _n( '', '', $tag_count, 'woocommerce' ) . ' ', '.</span></div>' );
    //echo strip_tags($product->get_tags( ' ', '<div class="product_meta"><span class="tagged_as">' . _n( '', '', $tag_count, 'woocommerce' ) . ' ', '.</span></div>'),'<div><span>');
}
//add_action( 'woocommerce_product_thumbnails', 'woocommerce_product_loop_tags', 10 );


add_filter('woocommerce_short_description', 'woocommerce_template_single_excerpt_link', 10, 1);
function woocommerce_template_single_excerpt_link($post_excerpt){
    if (is_product()) {
    	$post_excerpt = get_the_excerpt();
        //$post_excerpt = substr($post_excerpt, 0, 400) . '<span class="readmore"> &hellip; <a class="downto" href="#tab-description">' . __('Lire en détails', 'sage') . '</a></span>';
    	$post_excerpt = '<h3>' . substr($post_excerpt, 0, 400) . '</h3>';
    }
    return $post_excerpt;
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10);
//add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10);


// Remove up sells from after single product hook
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
// now we place it after product images gallery (sidebar)
//add_action( 'woocommerce_product_thumbnails', 'woocommerce_upsell_display', 20 );



/*
* We take the long description to generate a new excerpt 
* because we want to use the real excerpt field for a very short description above
*/

function woocommerce_template_single_product_sum(){
	if (is_product()) {
		$content = apply_filters( 'the_content', get_post_field('post_content') );
        $post_excerpt = substr($content, 0, 8000) . ' <a class="downto" href="#tab-description">' . __("Plus d'infos", 'sage') . '</a>';
        echo apply_filters( 'the_content', $post_excerpt );
        echo '<div class="fb-like" style="padding-bottom:30px;" data-href="https://www.facebook.com/emeraudeemotions/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>';
    }
}
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_product_sum', 10);


/** 
*  	product listing main catalogue page
* 	maybe we'll need this to hook on some actions :
* 	@link https://businessbloomer.com/woocommerce-visual-hook-guide-archiveshopcat-page/
*/


?>