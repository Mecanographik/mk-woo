export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    $('#js-parallax-window p.cta a').smoothScroll();

    // show / hide last 3rd testimonials
    $('#testimonial a.grid-item:gt(2)').hide();
	$('#testimonial #show-all').click(function(e) {
		e.preventDefault();
		$('#testimonial a.grid-item:gt(2)').toggle('show');
	});

  },
};
