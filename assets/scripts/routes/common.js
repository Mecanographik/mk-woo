export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

// Refills - begin centered nav
$(window).on("load resize",function() {
	//var more = document.getElementById("js-centered-more");
	var more = document.getElementsByClassName("more");
	if ($(more).length > 0) {
		var windowWidth = $(window).width();
		var moreLeftSideToPageLeftSide = $(more).offset().left;
		var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;
		if (moreLeftSideToPageRightSide < 330) {
			$(".more .submenu .sub-menu").removeClass("fly-out-right");
			$(".more .submenu .sub-menu").addClass("fly-out-left");
		}
		if (moreLeftSideToPageRightSide > 330) {
			$(".more .submenu .sub-menu").removeClass("fly-out-left");
			$(".more .submenu .sub-menu").addClass("fly-out-right");
		}
	}
	var menuToggle = $("#js-centered-navigation-mobile-menu").unbind();
	$("#js-centered-navigation-menu").removeClass("show");
	menuToggle.on("click", function(e) {
		e.preventDefault();
		$("#js-centered-navigation-menu").slideToggle(function(){
			if($("#js-centered-navigation-menu").is(":hidden")) {
				$("#js-centered-navigation-menu").removeAttr("style");
			}
		});
	});
}); 
// refills - end centerd nav


// Refills and smoothscroll - 
$(document).ready(function() {
 $('a.downto').smoothScroll();
 $('a.woocommerce-review-link').smoothScroll();
 $('#product-cover a.scroll-to-book').smoothScroll();

 // woocommerce pre check checkout paeg checkbox for shipping adress
 //$('#ship-to-different-address-checkbox').prop('checked', true);
 // woocommerce product tag lists : disable links on tags
 $('body.single-product span.tagged_as a').click(function(e) {
 e.preventDefault();
 });

 //begin parallaw window
 /*
 if ($("#js-parallax-window").length) {
    parallax();
  }
  */
});

/*
$(window).scroll(function() {
  if ($("#js-parallax-window").length) {
    parallax();
  }
});

function parallax(){
	if( $("#js-parallax-window").length > 0 ) {
	var plxBackground = $("#js-parallax-background");
	//var plxWindow = $("#js-parallax-window");
	//var plxWindowTopToPageTop = $(plxWindow).offset().top;
	var plxWindowTopToPageTop = 0;
	var windowTopToPageTop = $(window).scrollTop();
	var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;
	//var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
	//var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
	//var windowInnerHeight = window.innerHeight;
	//var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
	var plxSpeed = 0.25;

	plxBackground.css('top', - (plxWindowTopToWindowTop * plxSpeed) + 'px');
	}

	if( $("#testimonial").length > 0 ) {
	var NewplxBackground = $("#js-parallax-background-testimonial");
	var NewplxWindow = $("#testimonial");
	//var plxWindowTopToPageTop = $(plxWindow).offset().top;
	var NewplxWindowTopToPageTop = $(NewplxWindow).offset().top;
	var NewwindowTopToPageTop = $(window).scrollTop();
	var NewplxWindowTopToWindowTop = NewplxWindowTopToPageTop - NewwindowTopToPageTop;
	//var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
	//var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
	//var windowInnerHeight = window.innerHeight;
	//var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
	var NewplxSpeed = 0.25;

	NewplxBackground.css('top', - (NewplxWindowTopToWindowTop * NewplxSpeed) + 'px');
	}

}
*/
// refills parallax window end

  },
};
