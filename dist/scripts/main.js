/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/app/themes/mk-woo/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_jquery__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jquery_smooth_scroll_jquery_smooth_scroll_min__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_jquery_smooth_scroll_jquery_smooth_scroll_min___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_jquery_smooth_scroll_jquery_smooth_scroll_min__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_Router__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__routes_common__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__routes_home__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__routes_about__ = __webpack_require__(4);
/** import external dependencies */



/** import local dependencies */





/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
var routes = new __WEBPACK_IMPORTED_MODULE_2__util_Router__["a" /* default */]({
  /** All pages */
  common: __WEBPACK_IMPORTED_MODULE_3__routes_common__["a" /* default */],
  /** Home page */
  home: __WEBPACK_IMPORTED_MODULE_4__routes_home__["a" /* default */],
  /** About Us page, note the change from about-us to aboutUs. */
  aboutUs: __WEBPACK_IMPORTED_MODULE_5__routes_about__["a" /* default */],
});

/** Load Events */
jQuery(document).ready(function () { return routes.loadEvents(); });

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(0)))

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = {
  init: function init() {
    // JavaScript to be fired on the about us page
  },
};


/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {/* harmony default export */ __webpack_exports__["a"] = {
  init: function init() {
    // JavaScript to be fired on all pages
  },
  finalize: function finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired

// Refills - begin centered nav
$(window).on("load resize",function() {
	//var more = document.getElementById("js-centered-more");
	var more = document.getElementsByClassName("more");
	if ($(more).length > 0) {
		var windowWidth = $(window).width();
		var moreLeftSideToPageLeftSide = $(more).offset().left;
		var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;
		if (moreLeftSideToPageRightSide < 330) {
			$(".more .submenu .sub-menu").removeClass("fly-out-right");
			$(".more .submenu .sub-menu").addClass("fly-out-left");
		}
		if (moreLeftSideToPageRightSide > 330) {
			$(".more .submenu .sub-menu").removeClass("fly-out-left");
			$(".more .submenu .sub-menu").addClass("fly-out-right");
		}
	}
	var menuToggle = $("#js-centered-navigation-mobile-menu").unbind();
	$("#js-centered-navigation-menu").removeClass("show");
	menuToggle.on("click", function(e) {
		e.preventDefault();
		$("#js-centered-navigation-menu").slideToggle(function(){
			if($("#js-centered-navigation-menu").is(":hidden")) {
				$("#js-centered-navigation-menu").removeAttr("style");
			}
		});
	});
}); 
// refills - end centerd nav


// Refills and smoothscroll - 
$(document).ready(function() {
 $('a.downto').smoothScroll();
 $('a.woocommerce-review-link').smoothScroll();
 $('#product-cover a.scroll-to-book').smoothScroll();

 // woocommerce pre check checkout paeg checkbox for shipping adress
 //$('#ship-to-different-address-checkbox').prop('checked', true);
 // woocommerce product tag lists : disable links on tags
 $('body.single-product span.tagged_as a').click(function(e) {
 e.preventDefault();
 });

 //begin parallaw window
 /*
 if ($("#js-parallax-window").length) {
    parallax();
  }
  */
});

/*
$(window).scroll(function() {
  if ($("#js-parallax-window").length) {
    parallax();
  }
});

function parallax(){
	if( $("#js-parallax-window").length > 0 ) {
	var plxBackground = $("#js-parallax-background");
	//var plxWindow = $("#js-parallax-window");
	//var plxWindowTopToPageTop = $(plxWindow).offset().top;
	var plxWindowTopToPageTop = 0;
	var windowTopToPageTop = $(window).scrollTop();
	var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;
	//var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
	//var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
	//var windowInnerHeight = window.innerHeight;
	//var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
	var plxSpeed = 0.25;

	plxBackground.css('top', - (plxWindowTopToWindowTop * plxSpeed) + 'px');
	}

	if( $("#testimonial").length > 0 ) {
	var NewplxBackground = $("#js-parallax-background-testimonial");
	var NewplxWindow = $("#testimonial");
	//var plxWindowTopToPageTop = $(plxWindow).offset().top;
	var NewplxWindowTopToPageTop = $(NewplxWindow).offset().top;
	var NewwindowTopToPageTop = $(window).scrollTop();
	var NewplxWindowTopToWindowTop = NewplxWindowTopToPageTop - NewwindowTopToPageTop;
	//var plxBackgroundTopToPageTop = $(plxBackground).offset().top;
	//var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
	//var windowInnerHeight = window.innerHeight;
	//var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
	var NewplxSpeed = 0.25;

	NewplxBackground.css('top', - (NewplxWindowTopToWindowTop * NewplxSpeed) + 'px');
	}

}
*/
// refills parallax window end

  },
};

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(0)))

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {/* harmony default export */ __webpack_exports__["a"] = {
  init: function init() {
    // JavaScript to be fired on the home page
  },
  finalize: function finalize() {
    // JavaScript to be fired on the home page, after the init JS
    $('#js-parallax-window p.cta a').smoothScroll();

    // show / hide last 3rd testimonials
    $('#testimonial a.grid-item:gt(2)').hide();
	$('#testimonial #show-all').click(function(e) {
		e.preventDefault();
		$('#testimonial a.grid-item:gt(2)').toggle('show');
	});

  },
};

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(0)))

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__camelCase__ = __webpack_require__(8);
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 * ======================================================================== */



// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var Router = function Router(routes) {
  this.routes = routes;
};

Router.prototype.fire = function fire (route, fn, args) {
    if ( fn === void 0 ) fn = 'init';

  var fire = route !== '' && this.routes[route] && typeof this.routes[route][fn] === 'function';
  if (fire) {
    this.routes[route][fn](args);
  }
};

Router.prototype.loadEvents = function loadEvents () {
    var this$1 = this;

  // Fire common init JS
  this.fire('common');

  // Fire page-specific init JS, and then finalize JS
  document.body.className
    .toLowerCase()
    .replace(/-/g, '_')
    .split(/\s+/)
    .map(__WEBPACK_IMPORTED_MODULE_0__camelCase__["a" /* default */])
    .forEach(function (className) {
      this$1.fire(className);
      this$1.fire(className, 'finalize');
    });

  // Fire common finalize JS
  this.fire('common', 'finalize');
};

/* harmony default export */ __webpack_exports__["a"] = Router;


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// the most terrible camelizer on the internet, guaranteed!
/* harmony default export */ __webpack_exports__["a"] = function (str) { return ("" + (str.charAt(0).toLowerCase()) + (str.replace(/[\W_]/g, '|').split('|')
  .map(function (part) { return ("" + (part.charAt(0).toUpperCase()) + (part.slice(1))); })
  .join('')
  .slice(1))); };;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery Smooth Scroll - v2.1.2 - 2017-01-19
 * https://github.com/kswedberg/jquery-smooth-scroll
 * Copyright (c) 2017 Karl Swedberg
 * Licensed MIT
 */

!function(a){ true?!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (a),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):a("object"==typeof module&&module.exports?require("jquery"):jQuery)}(function(a){var b="2.1.2",c={},d={exclude:[],excludeWithin:[],offset:0,direction:"top",delegateSelector:null,scrollElement:null,scrollTarget:null,beforeScroll:function(){},afterScroll:function(){},easing:"swing",speed:400,autoCoefficient:2,preventDefault:!0},e=function(b){var c=[],d=!1,e=b.dir&&"left"===b.dir?"scrollLeft":"scrollTop";return this.each(function(){var b=a(this);if(this!==document&&this!==window)return!document.scrollingElement||this!==document.documentElement&&this!==document.body?void(b[e]()>0?c.push(this):(b[e](1),d=b[e]()>0,d&&c.push(this),b[e](0))):(c.push(document.scrollingElement),!1)}),c.length||this.each(function(){this===document.documentElement&&"smooth"===a(this).css("scrollBehavior")&&(c=[this]),c.length||"BODY"!==this.nodeName||(c=[this])}),"first"===b.el&&c.length>1&&(c=[c[0]]),c},f=/^([\-\+]=)(\d+)/;a.fn.extend({scrollable:function(a){var b=e.call(this,{dir:a});return this.pushStack(b)},firstScrollable:function(a){var b=e.call(this,{el:"first",dir:a});return this.pushStack(b)},smoothScroll:function(b,c){if(b=b||{},"options"===b)return c?this.each(function(){var b=a(this),d=a.extend(b.data("ssOpts")||{},c);a(this).data("ssOpts",d)}):this.first().data("ssOpts");var d=a.extend({},a.fn.smoothScroll.defaults,b),e=function(b){var c=function(a){return a.replace(/(:|\.|\/)/g,"\\$1")},e=this,f=a(this),g=a.extend({},d,f.data("ssOpts")||{}),h=d.exclude,i=g.excludeWithin,j=0,k=0,l=!0,m={},n=a.smoothScroll.filterPath(location.pathname),o=a.smoothScroll.filterPath(e.pathname),p=location.hostname===e.hostname||!e.hostname,q=g.scrollTarget||o===n,r=c(e.hash);if(r&&!a(r).length&&(l=!1),g.scrollTarget||p&&q&&r){for(;l&&j<h.length;)f.is(c(h[j++]))&&(l=!1);for(;l&&k<i.length;)f.closest(i[k++]).length&&(l=!1)}else l=!1;l&&(g.preventDefault&&b.preventDefault(),a.extend(m,g,{scrollTarget:g.scrollTarget||r,link:e}),a.smoothScroll(m))};return null!==b.delegateSelector?this.off("click.smoothscroll",b.delegateSelector).on("click.smoothscroll",b.delegateSelector,e):this.off("click.smoothscroll").on("click.smoothscroll",e),this}});var g=function(a){var b={relative:""},c="string"==typeof a&&f.exec(a);return"number"==typeof a?b.px=a:c&&(b.relative=c[1],b.px=parseFloat(c[2])||0),b};a.smoothScroll=function(b,d){if("options"===b&&"object"==typeof d)return a.extend(c,d);var e,f,h,i,j=g(b),k={},l=0,m="offset",n="scrollTop",o={},p={};j.px?e=a.extend({link:null},a.fn.smoothScroll.defaults,c):(e=a.extend({link:null},a.fn.smoothScroll.defaults,b||{},c),e.scrollElement&&(m="position","static"===e.scrollElement.css("position")&&e.scrollElement.css("position","relative")),d&&(j=g(d))),n="left"===e.direction?"scrollLeft":n,e.scrollElement?(f=e.scrollElement,j.px||/^(?:HTML|BODY)$/.test(f[0].nodeName)||(l=f[n]())):f=a("html, body").firstScrollable(e.direction),e.beforeScroll.call(f,e),k=j.px?j:{relative:"",px:a(e.scrollTarget)[m]()&&a(e.scrollTarget)[m]()[e.direction]||0},o[n]=k.relative+(k.px+l+e.offset),h=e.speed,"auto"===h&&(i=Math.abs(o[n]-f[n]()),h=i/e.autoCoefficient),p={duration:h,easing:e.easing,complete:function(){e.afterScroll.call(e.link,e)}},e.step&&(p.step=e.step),f.length?f.stop().animate(o,p):e.afterScroll.call(e.link,e)},a.smoothScroll.version=b,a.smoothScroll.filterPath=function(a){return a=a||"",a.replace(/^\//,"").replace(/(?:index|default).[a-zA-Z]{3,4}$/,"").replace(/\/$/,"")},a.fn.smoothScroll.defaults=d});

/***/ }),
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(2);
module.exports = __webpack_require__(3);


/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map